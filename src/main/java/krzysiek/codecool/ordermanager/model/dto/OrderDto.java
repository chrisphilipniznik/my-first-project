package krzysiek.codecool.ordermanager.model.dto;

import krzysiek.codecool.ordermanager.model.enums.OrderStatus;

public class OrderDto {

    public String name;
    public OrderStatus status;
    public String creationDate;
    public SimpleClientDto client;

}
