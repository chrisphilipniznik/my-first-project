package krzysiek.codecool.ordermanager.repository;

import krzysiek.codecool.ordermanager.model.Order;
import krzysiek.codecool.ordermanager.model.dto.OrderDto;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface OrderRepository extends JpaRepository<Order, String> {

    List<Order> findAll();

    void save(OrderDto orderDto);

}
